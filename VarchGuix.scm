;; CONFIGURACION DE OSVARCHA
(use-modules (gnu)
	     (nongnu packages linux)
	     (nongnu system linux-initrd)
	     (gnu services base)
	     (gnu services pm) ; Gestion de energia
	     (gnu services virtualization)
	     (gnu services dbus)
	     (gnu packages networking)
	     (gnu packages shells)
	     (gnu packages cups)
	     (gnu packages certs)
	     )

(use-service-modules cups desktop networking ssh linux xorg sddm)

;; (define %osvarcha-packages
;;   (append (list
;; 	   ;; hyprland
;; 	  %base-packages)))

(operating-system
  (locale "es_PE.utf8")
  (timezone "America/Lima")
  (keyboard-layout (keyboard-layout "latam"))
  (host-name "varchalap")

  (kernel linux-xanmod-lts)
  (initrd microcode-initrd)
  (firmware (list
	     realtek-firmware
	     linux-firmware))

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "osvarcha")
                  (comment "Osvarcha")
                  (group "users")
                  (home-directory "/home/osvarcha")
		  (shell
		   (file-append fish "/bin/fish"))
                  (supplementary-groups '("wheel" "netdev" "audio" "video" "kvm")))
                %base-user-accounts))

  (packages
   (append
    (map specification->package
	 '(
	   "hyprland"
	   "nss-certs"
	   ;; Herramientas si o si
	   "git-minimal"
	   ;; Virtualización
	   "qemu-minimal"
	   ;; Bluetooth
	   ;; "bluez-alsa" "bluez" "blueman"
	   ;; "pulseaudio"
	   ;; Firmware
	   ;; "rtl8723bt-firmware"
	   ;; Intel Drivers NonGNU
	   "intel-media-driver-nonfree" "i915-firmware"
	   ;; NTFS
	   "ntfs-3g"
	   ;; Desktop
	   "xdg-desktop-portal"
	   ;; Printers
	   "cups"
	   ))
    %base-packages
    ))
  
  ;; (packages
  ;;  (append
  ;;   (map specification->package
  ;; 	 '(
  ;; 	   "nss-certs"
  ;; 	   "git-minimal"
  ;; 	   "qemu-minimal"
  ;; 	   "intel-media-driver-nonfree"
  ;; 	   "ntfs-3g"
  ;; 	   "xdg-portal-tools"
  ;; 	   "xdg-desktop-portal"
  ;; 	   "cups"
  ;; 	   ))
    ;; (list
    ;;  nss-certs
    ;;  git-minimal
    ;;  qemu-minimal
    ;;  ;; rtl8723bt-firmware
    ;;  intel-media-driver-nonfree
    ;;  ;; i915-firware
    ;;  ntfs-3g
    ;;  xdg-portal-tools
    ;;  xdg-desktop-portal
    ;;  cups
    ;;  )
    ;; %osvarcha-packages))

  (services
   (append
    (list
     (service network-manager-service-type)
     (service wpa-supplicant-service-type)
     (service openssh-service-type)
     (service cups-service-type
	      (cups-configuration
	       (web-interface? #t)
	       (browsing? #t)
	       (default-paper-size "A4")
	       (extensions
		(list cups-filters foomatic-filters))))
     (service sddm-service-type
	      (sddm-configuration
	       (auto-login-user "osvarcha")
	       (display-server "wayland")
	       (numlock "on")
	       (minimum-uid 1000)
	       (theme "px-sddm-theme")
	       (xorg-configuration
		(xorg-configuration
		 (keyboard-layout keyboard-layout)))
	       ))
     ;; elogind
     (service elogind-service-type
	      (elogind-configuration
	       (handle-lid-switch-external-power 'suspend)))
     ;; Zram
     (service zram-device-service-type
	      (zram-device-configuration
	       (size (* 24 (expt 2 30)))
	       (compression-algorithm 'zstd)
	       (priority 100)))
     ;; Bluetooth
     (service bluetooth-service-type
	      (bluetooth-configuration
	       (auto-enable? #t)))
     ;; (simple-service 'dbus-extras
     ;; 		     dbus-root-service-type
     ;; 		     (list blueman))
     ;; Battery
     (service tlp-service-type
	      (tlp-configuration
	       ;; Gobernador de escalado en AC
	       (cpu-scaling-governor-on-ac (list "performance"))
	       ;; Gobernador de escalado en BAT
	       (cpu-scaling-governor-on-bat (list "powersave"))
	       ;; Activar el "turbo boost" en AC
	       (cpu-boost-on-ac? #t)
	       ;; Desactiva el "turbo boost" en BAT
	       (cpu-boost-on-bat? #f)
	       ;; #% en la bateria deberia comenzar a cargarse
	       (start-charge-thresh-bat0 40)
	       ;; #% en la bateria deberia dejar de de cargarse
	       (stop-charge-thresh-bat0 80)
	       ;; minimiza el #nucleos/hilos en AC
	       (sched-powersave-on-ac? #f)
	       ;; minimiza el #nucleos/hilos en BAT
	       (sched-powersave-on-bat? #t)))
     ;; Thermald
     (service thermald-service-type ; Thermald
	      (thermald-configuration
	       (adaptive? #t)))
     ;; Virtualización
     (service libvirt-service-type ; Virtualización
	      (libvirt-configuration
	       (unix-sock-group "users")))
     )
    %base-services))
  
  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid
                                  "235b6f79-f937-4bc2-ae7c-198a229df613"
                                  'btrfs))
                         (type "btrfs"))
                       (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "3A63-C0AC"
                                       'fat32))
                         (type "vfat")) %base-file-systems)))
